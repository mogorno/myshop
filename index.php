
<?php


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//echo "shop project";



require_once __DIR__ . '/vendor/autoload.php';
//$loader = new \Twig\Loader\FilesystemLoader('Shop/templates/');
//$twig = new \Twig\Environment($loader);
//echo $twig->render('index.html');
//die("1");

//          HEADER          //
use  shop\Header\Header\Header;
use  shop\Footer\Footer\Footer;
$head = new \shop\Header\Header();

echo $head->navigation();

//           CONTENT           //
$klein = new \Klein\Klein();

$klein->respond(['GET', 'POST'], '/', function () {

    $loader = new \Twig\Loader\FilesystemLoader('shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('index.html');
});

$klein->respond(['GET', 'POST'], '/category', function () {

    $loader = new \Twig\Loader\FilesystemLoader('shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('category.html');
});
$klein->respond(['GET', 'POST'], '/profile', function () {

    $loader = new \Twig\Loader\FilesystemLoader('shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('profile.html');
});
$klein->respond(['GET', 'POST'], '/order', function () {

    $loader = new \Twig\Loader\FilesystemLoader('shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('order.html');
});
$klein->respond(['GET', 'POST'], '/product', function () {

    $loader = new \Twig\Loader\FilesystemLoader('shop/templates/');
    $twig = new \Twig\Environment($loader);
    return  $twig->render('product.html');
});

$klein->dispatch();

//            FOOTER            //

$footer = new shop\Footer\Footer();
$footer->bottom();
